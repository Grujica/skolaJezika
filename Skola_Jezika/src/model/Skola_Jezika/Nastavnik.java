package model.Skola_Jezika;

public class Nastavnik {

	protected int id;
	protected String ime;
	protected String prezime;
	
	public Nastavnik() {}

	public Nastavnik(int id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	@Override
	public String toString() {
		return "Nastavnik [id=" + id + ", ime=" + ime + ", prezime=" + prezime
				+ "]";
	}

	
	
	
}
