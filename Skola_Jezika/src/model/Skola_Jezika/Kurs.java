package model.Skola_Jezika;

import java.util.ArrayList;

public class Kurs {

	protected int id;
	protected String jezik;
	protected String nivo;
	protected int cena;
	protected ArrayList<Student> studenti = new ArrayList<Student>();
	
    public ArrayList<Student> getStudenti() {
		return studenti;
	}

	public void setStudenti(ArrayList<Student> studenti) {
		this.studenti = studenti;
	}

	public Kurs() {}
	
	public Kurs(String jezik, String nivo, int cena) {
		super();
		this.jezik = jezik;
		this.nivo = nivo;
		this.cena = cena;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Kurs(int id, String jezik, String nivo, int cena) {
		super();
		this.id = id;
		this.jezik = jezik;
		this.nivo = nivo;
		this.cena = cena;
	}

	public String getJezik() {
		return jezik;
	}

	public void setJezik(String jezik) {
		this.jezik = jezik;
	}

	public String getNivo() {
		return nivo;
	}

	public void setNivo(String nivo) {
		this.nivo = nivo;
	}

	public int getCena() {
		return cena;
	}

	public void setCena(int cena) {
		this.cena = cena;
	}

	@Override
	public String toString() {
		return "Kurs [jezik=" + jezik + ", nivo=" + nivo + ", cena=" + cena
				+ "]";
	}

	
	
	
	
}
