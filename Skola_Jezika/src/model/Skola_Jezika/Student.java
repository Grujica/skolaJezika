package model.Skola_Jezika;

import java.util.ArrayList;

public class Student {

	protected int id;
	protected String ime;
	protected String prezime;
	protected ArrayList<Kurs> kursevi = new ArrayList<Kurs>();

	public ArrayList<Kurs> getKursevi() {
		return kursevi;
	}

	public void setKursevi(ArrayList<Kurs> kursevi) {
		this.kursevi = kursevi;
	}

	public Student(int id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", ime=" + ime + ", prezime=" + prezime
				+ "]";
	}
	
	
	
	
}

	

