package model.Skola_Jezika;

public class Skola {

	protected int PIB;
	protected String naziv;
	protected String adresa;
	protected int telefon;
	protected String mail;
	protected String sajt;
	protected String matBr;
	protected String ziroRacuna;
	
	public Skola() {
		
	}
	

	public Skola(int pIB, String naziv, String adresa, int telefon,
			String mail, String sajt, String matBr, String ziroRacuna) {
		super();
		PIB = pIB;
		this.naziv = naziv;
		this.adresa = adresa;
		this.telefon = telefon;
		this.mail = mail;
		this.sajt = sajt;
		this.matBr = matBr;
		this.ziroRacuna = ziroRacuna;
	}


	public Skola(String naziv, String adresa, int telefon, String mail,
			String sajt, int pIB, String matBr, String ziroRacuna) {
		super();
		this.naziv = naziv;
		this.adresa = adresa;
		this.telefon = telefon;
		this.mail = mail;
		this.sajt = sajt;
		PIB = pIB;
		this.matBr = matBr;
		this.ziroRacuna = ziroRacuna;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getSajt() {
		return sajt;
	}

	public void setSajt(String sajt) {
		this.sajt = sajt;
	}

	public int getPIB() {
		return PIB;
	}

	public void setPIB(int pIB) {
		PIB = pIB;
	}

	public String getMatBr() {
		return matBr;
	}

	public void setMatBr(String matBr) {
		this.matBr = matBr;
	}

	public String getZiroRacuna() {
		return ziroRacuna;
	}

	public void setZiroRacuna(String ziroRacuna) {
		this.ziroRacuna = ziroRacuna;
	}

	public String toString() {
		return "Skola [naziv=" + naziv + ", adresa=" + adresa + ", telefon="
				+ telefon + ", mail=" + mail + ", sajt=" + sajt + ", PIB="
				+ PIB + ", matBr=" + matBr + ", ziroRacuna=" + ziroRacuna + "]";
	}
	
	
	
}
