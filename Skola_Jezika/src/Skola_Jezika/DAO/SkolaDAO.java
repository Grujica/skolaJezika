package Skola_Jezika.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import model.Skola_Jezika.Skola;

	//CRUD operacije nad studentom
	public class SkolaDAO {
		
		public static Skola getSkolaById(Connection conn, int PIB) {
			Skola skola = null;
			try {
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt
						.executeQuery("SELECT naziv, adresa, telefon, mail, sajt, maticniBr, ziroRacun " +
								"FROM skole WHERE skola_PIB = " 
								+ PIB);

				if (rset.next()) {
					String naziv = rset.getString(1);
					String adresa = rset.getString(2);
					int telefon = rset.getInt(3);
					String mail = rset.getString(4);
					String sajt = rset.getString(5);
					String maticniBr = rset.getString(6);
					String ziroRacun = rset.getString(7);
					
					skola= new Skola(PIB, naziv, adresa, telefon, mail, sajt, maticniBr, ziroRacun);
				}
				rset.close();
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return skola;
		}
		
		public static Skola getSkolaByIndeks(Connection conn, String naziv) {
			Skola skola = null;
			try {
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt
						.executeQuery("SELECT skola_PIB, adresa, telefon, mail, sajt, maticniBr, ziroRacun " +
								"FROM skole WHERE naziv = '" 
								+ naziv + "'");

				if (rset.next()) {
					int PIB = rset.getInt(1);
					String adresa = rset.getString(2);
					int telefon = rset.getInt(3);
					String mail = rset.getString(4);
					String sajt = rset.getString(5);
					String maticniBr = rset.getString(6);
					String ziroRacun = rset.getString(7);
					
					skola = new Skola(PIB, naziv, adresa, telefon, mail, sajt, maticniBr, ziroRacun);
				}
				rset.close();
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return skola;
		
		}

		public static boolean add(Connection conn, Skola skola){
			boolean retVal = false;
			try {
				String update = "INSERT INTO studenti (naziv, adresa, telefon, mail, sajt, PIB, " +
						"maticniBr, ziroRacun) values (?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement pstmt = conn.prepareStatement(update);
				pstmt.setString(1, skola.getNaziv());
				pstmt.setString(2, skola.getAdresa());
				pstmt.setInt(3, skola.getTelefon());
				pstmt.setString(4, skola.getMail());
				pstmt.setString(5, skola.getSajt());
				pstmt.setString(6, skola.getMatBr());
				pstmt.setString(7, skola.getZiroRacuna());
				if(pstmt.executeUpdate() == 1){
					retVal = true;
				}
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		
		public static List<Skola> getAll(Connection conn) {
			List<Skola> retVal = new ArrayList<Skola>();
			try {
				String query = "SELECT skola_PIB, naziv, adresa, telefon, mail, sajt, matBr, ziroRacuna FROM skole ";
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt.executeQuery(query.toString());
				while (rset.next()) {
					int PIB = rset.getInt(1);
					String adresa = rset.getString(2);
					int telefon = rset.getInt(3);
					String mail = rset.getString(4);
					String sajt = rset.getString(5);
					String maticniBr = rset.getString(6);
					String ziroRacun = rset.getString(7);
				}
				rset.close();
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return retVal;
		}
		public static boolean update(Connection conn, Skola skola) {
			boolean retVal = false;
			try {
				String update = "UPDATE skole SET naziv=?, " +
						"adresa=?, telefon=?, mail=?, sajt=?, matBr=?, ziroRacuna=?  WHERE skola_PIB=?";
				PreparedStatement pstmt = conn.prepareStatement(update);
				pstmt.setString(1, skola.getNaziv());
				pstmt.setString(2, skola.getAdresa());
				pstmt.setInt(3, skola.getTelefon());
				pstmt.setString(4, skola.getMail());
				pstmt.setString(5, skola.getSajt());
				pstmt.setString(6, skola.getMatBr());
				pstmt.setString(7, skola.getZiroRacuna());
				if(pstmt.executeUpdate() == 1)
					retVal = true;
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		public static boolean delete(Connection conn, int PIB) {
			boolean retVal = false;
			try {
				String update = "DELETE FROM skole WHERE " +
						"skola_PIB = " + PIB;
				Statement stmt = conn.createStatement();
				if (stmt.executeUpdate(update) == 1)
					retVal = true;
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}

		
	}
	
	
	
