package Skola_Jezika.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Skola_Jezika.Kurs;


public class KursDAO {

	public static Kurs getKursById(Connection conn, int ksId) {
		Kurs kurs = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT jezik, nivo, cena " +
							"FROM kursevi WHERE kurs_id = " 
							+ ksId);

			if (rset.next()) {
				
				String jezik = rset.getString(1);
				String nivo = rset.getString(2);
				int cena = rset.getInt(3);
				
				
				kurs = new Kurs(jezik, nivo, cena);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return kurs;
	}
	
	public static Kurs getKursByIndeks(Connection conn, String jezik) {
		Kurs kurs = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT kurs_id, nivo " +
							"FROM kursevi WHERE jezik = '" 
							+ jezik + "'");

			if (rset.next()) {
				
			
				int id = rset.getInt(2);
				String nivo = rset.getString(2);
				int cena = rset.getInt(2);
				
				
				kurs = new Kurs(id, jezik, nivo, cena);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return kurs;
	}
	
	public static List<Kurs> getAll(Connection conn) {
		List<Kurs> retVal = new ArrayList<Kurs>();
		try {
			String query = "SELECT kurs_id, jezik, nivo, cena FROM kursevi ";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			while (rset.next()) {
				
				int id = rset.getInt(1);
				
				String jezik = rset.getString(2);
				String nivo = rset.getString(3);
				int cena = rset.getInt(3);
					
				Kurs kurs = new Kurs(id, jezik, nivo, cena);
			
				retVal.add(kurs);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static boolean add(Connection conn, Kurs kurs){
		boolean retVal = false;
		try {
			String update = "INSERT INTO kursevi (jezik, nivo " +
					"cena) values (?, ?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			
			pstmt.setString(1, kurs.getJezik());
			pstmt.setString(2, kurs.getNivo());
			pstmt.setInt(3, kurs.getCena());

			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean update(Connection conn, Kurs kurs) {
		boolean retVal = false;
		try {
			String update = "UPDATE kursevi SET jezik=?, " +
					"nivo=?, "+ "cena=? WHERE kurs_id=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			
			pstmt.setString(1, kurs.getJezik());
			pstmt.setString(2, kurs.getNivo());
			pstmt.setInt(3, kurs.getCena());
			pstmt.setInt(4, kurs.getId());
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean delete(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM kursevi WHERE " +
					"kurs_id = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
}

	

