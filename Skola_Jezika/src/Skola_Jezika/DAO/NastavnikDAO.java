package Skola_Jezika.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Skola_Jezika.Nastavnik;



public class NastavnikDAO {

	public static Nastavnik getNastavnikById(Connection conn, int id) {
		Nastavnik nastavnik = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT ime, prezime " +
							"FROM nastavnici WHERE nastavnik_id = " 
							+ id);

			if (rset.next()) {
				String ime = rset.getString(1);
				String prezime = rset.getString(2);
				
				
				nastavnik = new Nastavnik(id, ime, prezime);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nastavnik;
	}
	
	public static Nastavnik getNastavnikByZvanje(Connection conn, String ime) {
		Nastavnik nastavnik = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT nastavnik_id, prezime " +
							"FROM nastavnik WHERE zvanje = '" 
							+ ime + "'");

			if (rset.next()) {
				int id = rset.getInt(1);
				String prezime = rset.getString(2);
				
				
				nastavnik = new Nastavnik(id, ime, prezime);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nastavnik;
	}
	
	public static List<Nastavnik> getAll(Connection conn) {
		List<Nastavnik> retVal = new ArrayList<Nastavnik>();
		try {
			String query = "SELECT nastavnik_id, ime, prezime FROM nastavnici ";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			while (rset.next()) {
				int id = rset.getInt(1);
				String ime = rset.getString(2);
				String prezime = rset.getString(3);
				
				Nastavnik nastavnik = new Nastavnik(id, ime, prezime);
				
				retVal.add(nastavnik);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static boolean add(Connection conn, Nastavnik nastavnik){
		boolean retVal = false;
		try {
			String update = "INSERT INTO nastavnci ( ime, " +
					"prezime) values (?, ?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, nastavnik.getIme());
			pstmt.setString(2, nastavnik.getPrezime());
			
			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean update(Connection conn, Nastavnik nastavnik) {
		boolean retVal = false;
		try {
			String update = "UPDATE nastavnici SET zvanje=?, " +
					"ime=?, prezime=? WHERE nastavnik_id=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
		
			pstmt.setString(1, nastavnik.getIme());
			pstmt.setString(2, nastavnik.getPrezime());
		
			pstmt.setInt(4, nastavnik.getId());
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean delete(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM nastavnici WHERE " +
					"nastavnik_id = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
}

	
