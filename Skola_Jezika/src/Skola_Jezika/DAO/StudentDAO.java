package Skola_Jezika.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Skola_Jezika.Student;

public class StudentDAO {

	public static Student getStudentById(Connection conn, int id) {
		Student student = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT ime, prezime " +
							"FROM studenti WHERE student_id = " 
							+ id);

			if (rset.next()) {
				
				String ime = rset.getString(2);
				String prezime = rset.getString(3);
				
				
				student = new Student(id, ime, prezime);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return student;
	}
	
	public static Student getStudentByIndeks(Connection conn, String ime) {
		Student student = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT student_id, prezime " +
							"FROM studenti WHERE ime = '" 
							+ ime + "'");

			if (rset.next()) {
				int id = rset.getInt(1);
				
				String prezime = rset.getString(2);
				
				
				student = new Student(id, ime, prezime);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return student;
	}
	
	public static List<Student> getAll(Connection conn) {
		List<Student> retVal = new ArrayList<Student>();
		try {
			String query = "SELECT student_id, ime, prezime FROM studenti ";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			while (rset.next()) {
				int id = rset.getInt(1);
				
				String ime = rset.getString(2);
				String prezime = rset.getString(3);
				
					
				Student student = new Student(id, ime, prezime);
			
				retVal.add(student);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static boolean add(Connection conn, Student student){
		boolean retVal = false;
		try {
			String update = "INSERT INTO studenti (ime, " +
					"prezime) values (?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			
			pstmt.setString(1, student.getIme());
			pstmt.setString(2, student.getPrezime());

			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean update(Connection conn, Student student) {
		boolean retVal = false;
		try {
			String update = "UPDATE studenti SET ime=?, " +
					"prezime=? WHERE student_id=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			
			pstmt.setString(2, student.getIme());
			pstmt.setString(3, student.getPrezime());
			
			pstmt.setInt(5, student.getId());
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean delete(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM studenti WHERE " +
					"student_id = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
}

	

