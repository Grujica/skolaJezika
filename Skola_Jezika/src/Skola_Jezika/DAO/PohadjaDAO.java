package Skola_Jezika.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Skola_Jezika.Kurs;
import model.Skola_Jezika.Student;



	public class PohadjaDAO {

		
		
		public static List<Student> getStudentiByKursId(Connection conn, int id) {
			List<Student> retVal = new ArrayList<Student>();
			try {
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt
						.executeQuery("SELECT student_id FROM pohadja WHERE " +
								"kurs_id = " + id);

				while (rset.next()) {
					int studentId = rset.getInt(1);
					retVal.add(StudentDAO.getStudentById(conn, studentId));
				}
				rset.close();
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		public static boolean add(Connection conn, int studentId, int kursId){
			boolean retVal = false;
			try {
				String update = "INSERT INTO pohadja " +
						"(student_id, kurs_id) values (?,?)";
				PreparedStatement pstmt = conn.prepareStatement(update);
				pstmt.setInt(1, studentId);
				pstmt.setInt(2, kursId);
				if(pstmt.executeUpdate() == 1)
					retVal = true;
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		
		public static boolean delete(Connection conn, int studentId, int kursId) {
			boolean retVal = false;
			try {
				String update = "DELETE FROM pohadja WHERE student_id = " + 
						studentId + " AND kurs_id = " + kursId;
				Statement stmt = conn.createStatement();
				if(stmt.executeUpdate(update) == 1)
					retVal = true;
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		public static boolean deletePohadjanjaStudenta(Connection conn, int studentId) {
			boolean retVal = false;
			try {
				String update = "DELETE FROM pohadja WHERE student_id = " + studentId;
				Statement stmt = conn.createStatement();
				if(stmt.executeUpdate(update) != 0)
					retVal = true;
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		public static boolean deletePohadjanjaKursa(Connection conn, int kursId) {
			boolean retVal = false;
			try {
				String update = "DELETE FROM pohadja WHERE kurs_id = " + kursId;
				Statement stmt = conn.createStatement();
				if(stmt.executeUpdate(update) != 0)
					retVal = true;
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		//update svih pohadjanja jednog studenta
		public static boolean update(Connection conn, Student student){
			boolean retVal = false;
			try {
				//obrisemo prethodna pohadjaja
				retVal = deletePohadjanjaStudenta(conn, student.getId());
				//ako je brisanje uspelo idemo na dodavanje
				if(retVal){
					for (Kurs kurs : student.getKursevi()) {
						retVal = add(conn, student.getId(), kurs.getId());
						if(retVal == false)
							throw new Exception("Dodavanje nije valjalo");
					}
				} else {
					throw new Exception("Brisanje nije valjalo");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return retVal;
		}
		
		//update svih pohadjanja jednog predmeta
		public static boolean update(Connection conn, Kurs kurs){
			boolean retVal = false;
			try {
				//obrisemo prethodna pohadjaja
				retVal = deletePohadjanjaKursa(conn, kurs.getId());
				//ako je brisanje uspelo idemo na dodavanje
				if(retVal){
					for (Student student : kurs.getStudenti()) {
						retVal = add(conn, student.getId(), kurs.getId());
						if(retVal == false)
							throw new Exception("Dodavanje nije valjalo");
					}
				} else {
					throw new Exception("Brisanje nije valjalo");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return retVal;
		}
	}

	
