package Skola_Jezika.UI;

import java.util.List;

import model.Skola_Jezika.Student;
import Skola_Jezika.DAO.StudentDAO;
import Skola_Jezika.pomocnaKlasa.ScannerWrapper;



public class StudentUI {

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveStudente();
				break;
			case 2:
				unosNovogStudenta();
				break;
			case 3:
				izmenaPodatakaOStudentu();
				break;
			case 4:
				brisanjePodatakaOStudentu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa studentima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis svih Studenata");
		System.out.println("\tOpcija broj 2 - unos novog Studenta");
		System.out.println("\tOpcija broj 3 - izmena Studenta");
		System.out.println("\tOpcija broj 4 - brisanje Studenta");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	/** METODE ZA ISPIS STUDENATA ****/
	// ispisi sve studente
	public static void ispisiSveStudente() {
		List<Student> sviStudenti = StudentDAO.getAll(AplikacijaUI.conn);
		for (int i = 0; i < sviStudenti.size(); i++) {
			System.out.println(sviStudenti.get(i));
		}
	}

	/** METODE ZA PRETRAGU STUDENATA ****/
	// pronadji studenta
	public static Student pronadjiStudenta() {
		Student retVal = null;
		System.out.print("Unesi indeks studenta:");
		String stIme = ScannerWrapper.ocitajTekst();
		retVal = pronadjiStudenta(stIme);
		if (retVal == null)
			System.out.println("Student sa imenom " + stIme
					+ " ne postoji u evidenciji");
		return retVal;
	}

	// pronadji studenta
	public static Student pronadjiStudenta(String stIme) {
		Student retVal = StudentDAO.getStudentByIndeks(AplikacijaUI.conn,
				stIme);
		return retVal;
	}

	/** METODE ZA UNOS, IZMENU I BRISANJE STUDENATA ****/
	// unos novog studenta
	public static void unosNovogStudenta() {
		System.out.print("Unesi ime:");
		String stIme = ScannerWrapper.ocitajTekst();
		stIme = stIme.toUpperCase();
		while (pronadjiStudenta(stIme) != null) {
			System.out.println("Student sa imenom " + stIme
					+ " vec postoji");
			stIme = ScannerWrapper.ocitajTekst();
		}
		
		System.out.print("Unesi prezime:");
		String stPrezime = ScannerWrapper.ocitajTekst();
		
		Student st = new Student(0, stIme, stPrezime);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		StudentDAO.add(AplikacijaUI.conn, st);
	}

	// izmena studenta
	public static void izmenaPodatakaOStudentu() {
		Student st = pronadjiStudenta();
		if (st != null) {
			System.out.print("Unesi novo ime :");
			String stIme = ScannerWrapper.ocitajTekst();
			st.setIme(stIme);

			System.out.print("Unesi prezime:");
			String stPrezime = ScannerWrapper.ocitajTekst();
			st.setPrezime(stPrezime);
	
			StudentDAO.update(AplikacijaUI.conn, st);
		}
	}

	// brisanje studenta
	public static void brisanjePodatakaOStudentu() {
		Student st = pronadjiStudenta();
		if (st != null) {
			StudentDAO.delete(AplikacijaUI.conn, st.getId());
		}
	}

}

	

