package Skola_Jezika.UI;

import java.util.List;

import model.Skola_Jezika.Kurs;

import Skola_Jezika.DAO.KursDAO;

import Skola_Jezika.pomocnaKlasa.ScannerWrapper;

public class KursUI {

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveKurseve();
				break;
		
			
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa kursevima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis svih kurseva");
		
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	
	
	
	/** METODE ZA ISPIS KURSA ****/
	// ispisi sve kurseve
	public static void ispisiSveKurseve() {
		List<Kurs> kursevi = KursDAO.getAll(AplikacijaUI.conn);
		for (int i = 0; i < kursevi.size(); i++) {
			System.out.println(kursevi.get(i));
		}
	}

	/** METODE ZA PRETRAGU KURSA ****/
	// pronadji kurs
	public static Kurs pronadjiKurs() {
		Kurs retVal = null;
		System.out.print("Unesi jezik kurs:");
		int ksId = ScannerWrapper.ocitajCeoBroj();
		retVal = pronadjiKurs(ksId);
		if (retVal == null)
			System.out.println("Kurs sa imenom " + ksId
					+ " ne postoji u evidenciji");
		return retVal;
	}

	// pronadji kurs
	public static Kurs pronadjiKurs(int ksId) {
		Kurs retVal = KursDAO.getKursById(AplikacijaUI.conn,
				ksId);
		return retVal;
	}

}

	

	