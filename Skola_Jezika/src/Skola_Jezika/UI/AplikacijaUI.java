package Skola_Jezika.UI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Skola_Jezika.pomocnaKlasa.ScannerWrapper;


public class AplikacijaUI {

public static Connection conn;
	
	static {
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");

			// konekcija
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/skolajezika", 
					"root", "grujo");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)  {
		int odluka = -1;
		while (odluka != 0) {
			AplikacijaUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				SkolaUI.menu();
				break;
			case 2:
				NastavnikUI.menu();
				break;
			case 3:
				StudentUI.menu();
				break;
			case 4:
				KursUI.menu();
				break;
			case 5:
				PohadjaUI.menu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Skola jezika - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa skolama");
		System.out.println("\tOpcija broj 2 - Rad sa nastavnicima");
		System.out.println("\tOpcija broj 3 - Rad sa studentima");
		System.out.println("\tOpcija broj 4 - Rad sa kursevima");
		System.out.println("\tOpcija broj 5 - Pohadja");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

}

	
	

