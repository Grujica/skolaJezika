package Skola_Jezika.UI;

import java.util.List;

import Skola_Jezika.DAO.SkolaDAO;
import Skola_Jezika.pomocnaKlasa.ScannerWrapper;
import model.Skola_Jezika.Skola;


public class SkolaUI {

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveSkole();
				break;
			case 2:
				unosNoveSkole();
				break;
			case 3:
				izmenaPodatakaOSkoli();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa skolama - opcije:");
		System.out.println("\tOpcija broj 1 - ispis svih skola");
		System.out.println("\tOpcija broj 2 - unos nove skole");
		System.out.println("\tOpcija broj 3 - izmena skole");
		
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	/** METODE ZA ISPIS SKOLA ****/
	// ispisi sve skole
	public static void ispisiSveSkole() {
		List<Skola> skole = SkolaDAO.getAll(AplikacijaUI.conn);
		for (int i = 0; i < skole.size(); i++) {
			System.out.println(skole.get(i));
		}
	}

	/** METODE ZA PRETRAGU SKOLE ****/
	// pronadji skolu
	public static Skola pronadjiSkolu() {
		Skola retVal = null;
		System.out.print("Unesi naziv skoole:");
		String skNaziv = ScannerWrapper.ocitajTekst();
		retVal = pronadjiSkolu(skNaziv);
		if (retVal == null)
			System.out.println("Skola sa nazivom " + skNaziv
					+ " ne postoji u evidenciji");
		return retVal;
	}

	// pronadji skola
	public static Skola pronadjiSkolu(String skNaziv) {
		Skola retVal = SkolaDAO.getSkolaByIndeks(AplikacijaUI.conn,
				skNaziv);
		return retVal;
	}

	/** METODE ZA UNOS, IZMENU I SKOLE ****/
	// unos nove Skole
	public static void unosNoveSkole() {
		System.out.print("Unesi naziv:");
		String skNaziv = ScannerWrapper.ocitajTekst();
		skNaziv = skNaziv.toUpperCase();
		while (pronadjiSkolu(skNaziv) != null) {
			System.out.println("Skola sa nazivom " + skNaziv
					+ " vec postoji");
			skNaziv = ScannerWrapper.ocitajTekst();
		}
		System.out.print("Unesi adresu:");
		String skAdresa = ScannerWrapper.ocitajTekst();
		System.out.print("Unesi telefon:");
		int skTelefon = ScannerWrapper.ocitajCeoBroj();
		System.out.print("Unesi mail:");
		String skMail = ScannerWrapper.ocitajTekst();
		System.out.print("Unesi sajt:");
		String skSajt = ScannerWrapper.ocitajTekst();

		System.out.print("Unesi maticniBroj:");
		String skMatBr = ScannerWrapper.ocitajTekst();
		System.out.print("Unesi ziro racun:");
		String skZiroRacuna = ScannerWrapper.ocitajTekst();
		
		Skola sk = new Skola(0, skNaziv, skAdresa, skTelefon, skMail, skSajt, skMatBr, skZiroRacuna);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		SkolaDAO.add(AplikacijaUI.conn, sk);
	}

	// izmena skole
	public static void izmenaPodatakaOSkoli() {
		Skola sk = pronadjiSkolu();
		if (sk != null) {
			System.out.print("Unesi novi naziv :");
			String skNaziv = ScannerWrapper.ocitajTekst();
			sk.setNaziv(skNaziv);

			System.out.print("Unesi adresu :");
			String skAdresa = ScannerWrapper.ocitajTekst();
			sk.setAdresa(skAdresa);

			System.out.print("Unesi telefon:");
			int skTelefon = ScannerWrapper.ocitajCeoBroj();
			sk.setTelefon(skTelefon);
	
			System.out.print("Unesi mail:");
			String skMail = ScannerWrapper.ocitajTekst();
			sk.setMail(skMail);

			System.out.print("Unesi sajt:");
			String skSajt = ScannerWrapper.ocitajTekst();
			sk.setSajt(skSajt);
			
			System.out.print("Unesi maticni Broj:");
			String skMatBr = ScannerWrapper.ocitajTekst();
			sk.setMatBr(skMatBr);
		
			
			System.out.print("Unesi ziroRacun:");
			String skZiroRacuna = ScannerWrapper.ocitajTekst();
			sk.setZiroRacuna(skZiroRacuna);
		}
		
	}

	
	
	

}

	
	

