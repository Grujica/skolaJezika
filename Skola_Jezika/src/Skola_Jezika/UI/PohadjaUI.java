package Skola_Jezika.UI;

import java.util.List;

import Skola_Jezika.DAO.PohadjaDAO;
import Skola_Jezika.pomocnaKlasa.ScannerWrapper;
import model.Skola_Jezika.Kurs;
import model.Skola_Jezika.Student;


public class PohadjaUI {

	private static void ispisiMenu() {
		System.out.println("Rad sa kuresevima - studenta - opcije:");
	
		System.out.println("\tOpcija broj 1 - studenti koji pohadjaju predmet");
		
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				
				break;
			case 1:
				ispisiStudenteZaKurs();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	private static void ispisiStudenteZaKurs() {
		
		Kurs kurs = KursUI.pronadjiKurs();
		if (kurs != null) {
			List<Student> studenti = PohadjaDAO.getStudentiByKursId(
					AplikacijaUI.conn, kurs.getId());
	
			for (Student s : studenti) {
				System.out.println(s);
			}
		}
	}
}

	