package Skola_Jezika.UI;

import java.util.List;

import model.Skola_Jezika.Nastavnik;
import Skola_Jezika.DAO.NastavnikDAO;
import Skola_Jezika.pomocnaKlasa.ScannerWrapper;



public class NastavnikUI {

	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveNastavnike();
				break;
			case 2:
				unosNovogNastavnika();
				break;
			case 3:
				izmenaPodatakaONastavniku();
				break;
			case 4:
				brisanjePodatakaONastavniku();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa nastavnicima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis svih Nastavnika");
		System.out.println("\tOpcija broj 2 - unos novog Nastavnika");
		System.out.println("\tOpcija broj 3 - izmena Nastavnika");
		System.out.println("\tOpcija broj 4 - brisanje Nastavnika");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	/** METODE ZA ISPIS NASTAVNIKA ****/
	// ispisi sve nastavnike
	public static void ispisiSveNastavnike() {
		List<Nastavnik> sviNastavnici = NastavnikDAO.getAll(AplikacijaUI.conn);
		for (int i = 0; i < sviNastavnici.size(); i++) {
			System.out.println(sviNastavnici.get(i));
		}
	}

	/** METODE ZA PRETRAGU NASTAVNIKA ****/
	// pronadji nastavnika
	public static Nastavnik pronadjiNastavnika() {
		Nastavnik retVal = null;
		System.out.print("Unesi ime Nastavvnika:");
		String nsIme = ScannerWrapper.ocitajTekst();
		retVal = pronadjiNastavnika(nsIme);
		if (retVal == null)
			System.out.println("Nastavnik sa imenom " + nsIme
					+ " ne postoji u evidenciji");
		return retVal;
	}

	// pronadji nastavnika
	public static Nastavnik pronadjiNastavnika(String nsIme) {
		Nastavnik retVal = NastavnikDAO.getNastavnikByZvanje(AplikacijaUI.conn,
				nsIme);
		return retVal;
	}

	/** METODE ZA UNOS, IZMENU I BRISANJE NASTAVNIKA ****/
	// unos novog nastavnika
	public static void unosNovogNastavnika() {
		System.out.print("Unesi ime:");
		String nsIme = ScannerWrapper.ocitajTekst();
		nsIme = nsIme.toUpperCase();
		while (pronadjiNastavnika(nsIme) != null) {
			System.out.println("Nastavnik sa imenom " + nsIme
					+ " vec postoji");
			nsIme = ScannerWrapper.ocitajTekst();
		}
		System.out.print("Unesi id:");
		int nsId = ScannerWrapper.ocitajCeoBroj();
		System.out.print("Unesi prezime:");
		String nsPrezime = ScannerWrapper.ocitajTekst();
		

		Nastavnik ns = new Nastavnik(nsId, nsIme, nsPrezime);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		NastavnikDAO.add(AplikacijaUI.conn, ns);
	}

	// izmena nastavnika
	public static void izmenaPodatakaONastavniku() {
		Nastavnik ns = pronadjiNastavnika();
		if (ns != null) {
			System.out.print("Unesi novi id :");
			int nsId = ScannerWrapper.ocitajCeoBroj();
			ns.setId(nsId);

			System.out.print("Unesi ime :");
			String nsIme = ScannerWrapper.ocitajTekst();
			ns.setIme(nsIme);

			System.out.print("Unesi prezime:");
			String nsPrezime = ScannerWrapper.ocitajTekst();
			ns.setPrezime(nsPrezime);

			NastavnikDAO.update(AplikacijaUI.conn, ns);
		}
	}

	// brisanje nastavnika
	public static void brisanjePodatakaONastavniku() {
		Nastavnik ns = pronadjiNastavnika();
		if (ns != null) {
			NastavnikDAO.delete(AplikacijaUI.conn, ns.getId());
		}
	}

}

	
